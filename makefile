# Default to ~/.vscode
VSCODE := $(shell								\
	if [ -d $(HOME)/.vscode ]; then				\
		echo .vscode;							\
	elif [ -d $(HOME)/.vscode-oss ]; then		\
		echo .vscode-oss;						\
	elif [ -d $(HOME)/.vscode-server ]; then	\
		echo .vscode-server;					\
	fi)

EXTENSIONS := ~/$(VSCODE)/extensions

# Used by subfolder make
export ASSIGNMENT_NAME := $(shell basename $(PWD))
export EXTENSION_PATH := $(EXTENSIONS)/$(ASSIGNMENT_NAME)
export ASSIGNMENT_PATH := $(PWD)
export ABS_EXTENSION_PATH := $(HOME)/$(VSCODE)/extensions/$(ASSIGNMENT_NAME)

# Set global node packages to install without sudo
export NPM_PACKAGES := $(HOME)/.npm-packages
export NODE_PATH := $(NPM_PACKAGES)/lib/node_modules:$(NODE_PATH)
export PATH := $(NPM_PACKAGES)/bin:$(HOME)/.local/bin:$(PATH)

all: ~/.npmrc $(EXTENSION_PATH)/src/services.js $(EXTENSION_PATH)/node_modules

$(EXTENSION_PATH): .vsce/extension
	cp $< $@ -R -T

~/service-manager:
	cd ~ && git clone https://gitlab.com/classroomcode/vsce-assignment/service-manager

update-service-manager: ~/service-manager
	cd $^ && python3 setup.py install --user

# Execute on first time setup and expose in $PATH
~/.npmrc:
	echo prefix=$(HOME)/.npm-packages > $@
	$(MAKE) node_packages

node_packages: ~/.npm-packages/bin/npm ~/.npm-packages/bin/webpack ~/.npm-packages/bin/webpack-cli

~/.npm-packages/bin/npm:
	npm install -g npm@latest

~/.npm-packages/bin/webpack:
	npm install -g webpack

~/.npm-packages/bin/webpack-cli:
	npm install -g webpack-cli

$(EXTENSION_PATH)/node_modules: $(EXTENSION_PATH) ~/.npmrc
	$(MAKE) -C $(EXTENSION_PATH)

$(EXTENSION_PATH)/src/services.js: $(EXTENSION_PATH) ~/.local/bin/virtualenv update-service-manager
	$(MAKE) -C .vsce

~/.local/bin/virtualenv:
	pip3 install virtualenv --user

.PHONY: clean update-service-manager

# Remove the venv and vscode extension bound to this assignment instance
clean:
	$(MAKE) clean -C ./.vsce

# Reset build environment. Useful if `make ~/.npmrc` fails.
purge: clean
	rm ~/.npmrc
	rm -rf ~/service-manager
	rm -rf ~/.npm-packages

~/.local/bin/watchmedo:
	pip3 install argh --user
	pip3 install watchdog --user

# Watch files and execute extension build pipeline on file change
develop: ~/.local/bin/watchmedo
	watchmedo auto-restart -R -d "./.vsce/extension/src" $(MAKE)
