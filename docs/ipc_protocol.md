# Inter-Process Communication

vsce-assignment extensions uses a microservice architecture. Instead of developing a monolithic program that's difficult to maintain and change, we implement our extension objectives by through plug-and-play microservices. For example, if we want to present control flow graphs, we register py2cfg as an extension dependency, then implement the front end javascript code to request a CFG element. This document describes the process of registering a service as a dependency of an assignment instance, building, and communicating with the service

## Service Registration
Registering a service begins with editing `.vsce/services.json` file. The JSON document must look something like

```json
// .vsce/services.json
{
    "version": 0.1,
    "services": [
        {
            "url": "https://gitlab.com/classroomcode/minimal-vsce-microservice",
            "addr": ["localhost", "8000"],
            "args": [] // optional
        },
        {
            "url": "https://gitlab.com/classroomcode/py2cfg",
            "addr": ["localhost", "8001"],
            "args": ["--directory", "${ASSIGNMENT_PATH}"]
        },
        {
            "url": "https://gitlab.com/classroomcode/minimal-websocket-service",
            "addr": ["localhost", "8002"],
        },
        {
            "url": "https://gitlab.com/classroomcode/minimal-stream-microservice",
            "streams": ["stdin", "stdout", "stderr"]
        }
    ]
}
```
This tells the extension that we will be running 4 services, obtained by cloning from the `url` property.

## Service Types
The service repo must contain a "service-manifest.json" file which contains information about how to run the service. Every manifest file must contain a `version`, `type` and `run` property.

### Stream Service
```json
// classroomcode/minimal-stream-microservice
{
    "version": 0.1,
    "type": "stream",
    "run": [...],
}
```

**Note**: classroomcode/minimal-websocket-microservice doesn't exist yet...

The simplest service is a stream service. Stream services are primitive. They process inputs from `stdin` and outputs to `stdout` and/or `stderr`. It's up to the extension to decide what to pass to `stdin` and how to present the outputs from `stdout` and/or `stderr`
### HTTP Service
```json
// py2cfg/service-manifest.json
{
    "version": 0.1,
    "type": "http",
    "run": ["python3", "-m", "py2cfg.server"],
    "test": ["python3", "tests/test_model.py"], // optional
    "contributes":[
        {
            "route": "/build",
            "method": "build_cfg"
        }
    ]
}
```

For reasons that will be discussed later, HTTP services need only to respond to GET requests. It contains an additional `contributes` property that specifies a list of objects.

### Websocket Service
```json
// classroomcode/minimal-websocket-microservice
{
    "version": 0.1,
    "type": "ws",
    "run": [...],
    "contributes": [
        "method1",
        "method2",
    ]
}
```
**Note**: classroomcode/minimal-websocket-microservice doesn't exist yet...

For websocket services, the `contributes` property is a list of methods that a client may subscribe to.

## Service communication
Stream services communicate by piping bytes. No additional formatting/validation is performed. For Websocket and HTTP services, we use JSON-RPC 2.0 specification. The details are described here https://www.jsonrpc.org/specification

JSON RPC protocol is not exposed at the developer level. See RPC-API documentation.

## Building
Services may be implemented in any language. Just like the extension itself, services are built by calling `'make'`. After cloning, the makefile for that repo is invoked as a subprocess. The service makefile builds the service and installs any dependencies the service requires. While the service doesn't have to be implemented in python, `'make'` is executed within a dedicated python environment. When called, the service runs within the same python environment.
