### SSH to MST Linux box in Visual Studio Code
1. The tutorial is in "ssh-install" file

### Open up the command line
1. Drag up the lower bar. It may take a while for it to load.
2. Click on the "Terminal" tab

## Install the `sample-vsce-assignment` extension

![](./extension-install-part1.gif)

```bash
$ git clone https://gitlab.com/classroomcode/sample-vsce-extension
$ cd sample-vsce-extension
$ make
```
1. It may take a long time to install the first time. It should be faster after subsequent installs.
2. It may appear to freeze. Just wait.

## Verify installation was successful

![](./extension-install-part2.gif)

1. Once you see some yellow text on the screen, installation is complete.
2. To verify that the extension was installed successfully, click on Extensions in the side menu and verify that `sample-vsce-assignment` is in the list of extensions installed remotely.
3. Press `Control + Shift + P` to bring up the developer command menu.
4. Select `Hello World` and a second panel should open up that displays `hello, world!`