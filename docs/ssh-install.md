### GIF Walkthrough
![](./vscode-ssh.gif)

### Install Visual Studio Code
1. If you're using your own computer, you should already be familiar with
    installing programs on your OS.
2. Go to https://code.visualstudio.com/ to download the package.

### Install the SSH Extension
1. Click on the extension box and search "SSH". Install Microsoft's Remote SSH extension.
2. Verify The extension is installed by the green square on the lower left corner. If it doesn't appear, try pressing "Ctrl + Shift + P" and select "Reload Window"

### Turn on Auto-save and configure Remote.SSH setting
1. File -> Preferences -> Settings -> Search Remote.SSH
2. Click the box next to "Remote.SSH: Lockfiles in Tmp"

### Configure SSH parameters
1. Go to https://it.mst.edu/services/linux/hostnames/ and pick a host in the 'Virtual CS Lab 213 column'
2. Click the green square on the lower left corner. A prompt should appear at the top of the screen. Click on Remote-SSH: Configure file. Select the folder that corresponds to your home directory. Paste this in.
```
Host <HOSTNAME>
  HostName <HOSTNAME>
  User um-ad\<YOUR USER ID>
```
3. Replace `<HOSTNAME>` with selected host and `<YOUR USER ID>` with your user id.

### Connect to MST Linux machine
1. If you're off campus network, make sure you're connected over VPN.
2. Click on green square and select "Connect Window to Host".
3. Select Linux as the operating system and enter your password. It may take a while to connect
4. If you see `um-ad\<your userid>@<hostname>` in the corner, you've successfuly set up vscode's SSH extension.

