import 'regenerator-runtime/runtime' // support async/await
import * as vscode from "vscode"
import * as fs from "fs"
import {services, path, env, name, start_command} from "./services"
import * as ospath from "path"
import {execFile, execFileSync, spawn} from "child_process"
import * as http from "http"

let webview = null;
let diff_webview = null;
let script;
let service_api = {};
let deactivate_services = null;
let service_info = {};
let activated = false;
let grade_sh = null;
let gradeable = true;

process.env.PATH = `${process.env.HOME}/.local/bin:${process.env.PATH}`

function
_activate(context)
{
	script = fs.readFileSync(
		ospath.join(path, "build_webview", "webview.js")
	)
	for (let service of services) {
		if (service.type === "http" || service.type === "ws") {
			console.log(`Starting ${service.name}`)
			exec_service(
				service
			).then(
				([service, port]) =>
				{
					console.log(`Service ${service.name} on ${port}`)
					if (service.type === "http") {
						http_service_init(service, "localhost", port);
					}
					else if (service.type === "ws") {
						ws_service_init(service, "localhost", port);
					}
					service_info[service.url] = {status: "OK"}
				}
			).catch(
				([service, error]) =>
				{
					console.log(`Failed to start ${service.name}`)
					service_info[service.url] = {error};
				}
			);
		}
		else {
			/* TODO handle stream init */
			console.log(service);
		}
	}
	vscode.window.onDidChangeTextEditorSelection((event) => 
	{
		if (webview) {
			
			event.filename = ospath.basename(event.textEditor.document.uri.path);
			event.filepath = event.textEditor.document.uri.path;
			webview.webview.postMessage({
				event:"onDidChangeTextEditorSelection",
				data: event,
			});
		}
	})
	context.subscriptions.push(vscode.commands.registerCommand(start_command, () =>
	{
		webview = webview_create(context)
	}));
	deactivate_services = () =>
	{
		for (let service of services) {
			execFileSync("vsce-service-stop", [env.ASSIGNMENT_PATH, service.url]);
		}
	}
}

export function
activate(context)
{
	if (is_assignment_directory()) {
		/* Start the service manager daemon if it isn't running already */
		spawn("vsce-start", {detached:true, shell:true});
		(new Promise(async (resolve, failure) =>
		{

			let vsce_status_process = execFile("vsce-status", ["-s"], {timeout:5000});
			let timeout = 5000;			
			vsce_status_process.stdout.setEncoding("utf-8");
			vsce_status_process.stdout.on("data",
				(message) =>
				{
					if (parseInt(message) === 1) {
						try {
							vsce_status_process.kill("SIGINT");
						} catch {};
						
						resolve();
					}
				}
			);
			while (timeout -= 500 > 0) {
				try {
                	vsce_status_process.stdin.write("\n");
				} catch{
					return;
				}
				await new Promise((r) => setTimeout(r, 500));
			}
			failure();
		})).then( 
			() =>
			{
				/* Shield from spurious wakeup */
				if (! activated) {
					activated = true;
					console.log("VSCE-ServiceManager: OK");
					_activate(context);
					vscode.window.showInformationMessage(`Activated ${name}`);
				}
			} 
		).catch(
			() =>
			{
				if (! activated) {
					vscode.window.showErrorMessage("Failed to start service manager")
				}
			}
		);
	}
}


function
is_assignment_directory()
{
	return vscode.workspace.workspaceFolders[0].uri.path === env.ASSIGNMENT_PATH;
}

export function
deactivate()
{
	if (deactivate_services) deactivate_services();
}

/* Request service to be started and return the port the service is listening on */
function
exec_service(service)
{
	let args = [env.ASSIGNMENT_PATH, service.url, service.path, service.run];
	return new Promise((resolve, failure) =>
	{
		execFile("vsce-service-start", args, (error, stdout) =>
		{
			if (error) {
				failure([service, error])
			}
			else {
				let response = JSON.parse(stdout);
				if (response.error) {
					failure([service, response.error]);
				}
				resolve([service, response.result]);
			}
		});
	});
}

function
webview_create(context)
{
	if (! webview) {
		webview = vscode.window.createWebviewPanel(
			"name",
			"",
			vscode.ViewColumn.Two,
			{
				enableScripts: true,
				localResourceRoots:[
					vscode.Uri.file(env.ASSIGNMENT_PATH)
				]
			}
		);
		/* Handle webview messages */
		webview.webview.onDidReceiveMessage((message) =>
		{
			switch (message.event) {
				/* Provide additional info about service */
				case "status": {
					let {provider} = message.data;
					webview.webview.postMessage({
						event: "status",
						data: service_info[provider],
					});
					break;
				}
				/* Since webview operates in separate context that doesn't have
				access to http, forward the webview calls. */
				case "call": {
					let {provider, method, params, call_id} = message.data;
					if (! provider in service_api) {
						webview.webview.postMessage({
							event: "return",
							data: {
								call_id,
								error: 2,
								data: service_info[url],
							}
						});
					}
					console.debug(`Forwarding call to ${provider}`);
					service_api[provider][method](params).then(
						(result) => webview.webview.postMessage({
							event: "return",
							data: {
								call_id,
								error: 0,
								data: result,
							}
						})
					).catch(
						(error) => webview.webview.postMessage({
							event: "return",
							data: {
								call_id,
								error: 1,
								data: error,
							}
						})
					);
					break;
				}
				/* Handle grade.sh queries/commands. */
				case "diff": {
					let filename = message.data.replace("_test.txt", ".html");
					let filepath = `${env.ASSIGNMENT_PATH}/stdio_tests/diffs/${filename}`
					let html = fs.readFileSync(filepath, "utf-8");
					webview.webview.postMessage({
						event: "onDiff",
						data: {
							input: message.data,
							diff: html,
							src:vscode.Uri.file(filepath),
						}
					});
					break;
				}
				case "grade": {
					if (gradeable) {
						init_grade_sh().stdin.write("\n");
						gradeable = false;
					}
					break;
				}
				case "lsof": {
					let data =  "lsof " + message.data.join(" ");
					console.log("grade.sh: < "+data);
					init_grade_sh().stdin.write(data + "\n");
					break;
				}
				case "test": {
					if (gradeable) {
						gradeable = false;
						let command = "test " + message.data.join(" ");
						console.log("grade.sh: < " + command);
						init_grade_sh().stdin.write(command + "\n");
					}
					break;
				}
			}
		});

		webview.webview.html = webview_html();
		webview.onDidDispose(() => webview = null);
	}
	return webview
}

function
webview_html()
{
	return `
		<!DOCTYPE html> 
		<head>
			<meta charset="UTF-8">
			<meta 
					http-equiv="Content-Security-Policy"
					content="
						default-src 'none';
						img-src https:;
						script-src 'self' 'unsafe-eval' 'unsafe-inline' vscode-resource:;
						style-src 'self' vscode-resource: 'unsafe-inline';
						object-src 'self' vscode-resource: 'unsafe-inline';
					"
				>
			<script>
				window.vscode = acquireVsCodeApi();
			</script>
			</head>
		<body>
			<div id="root"></div>
			<script>${script}</script>
		</body>
	`;
}

function
http_service_init(service, host, port)
{	
	let http_service = {};
	let msgid = 0;

	for (let {route, method} of service.contributes) {
		http_service[method] = (params=null) => 
		{
			let thisid = msgid;
			return new Promise((resolve, failure) => 
			{
				let data = {
					jsonrpc: "2.0",
					method,
					params:params,
					id: msgid ++,
				};

				get_request(
					host, port, route, data
				).then(
					(data) =>
					{
						if (data.result) {
							resolve(data.result)
						}
						else {
							failure(data.error)
						}
					}
				).catch(failure);
			});
		}
	}
	service_api[service.name] = http_service;
}

function
ws_service_init(service)
{
	let ws_service = {};
	for (let method of service.contributes) {
		/* TODO */
		ws_service[method] = (...params) =>
		{
			
		}
	}
	return ws_service
}

function
stream_service_init(service)
{
	let stream_service = {};
	for (let method of service.contributes) {
		/* TODO */
		stream_service[method] = (...params) =>
		{
			
		}
	}
	return stream_service
}


function
get_request(hostname, port, path, data)
{
	let string = JSON.stringify(data);
	return new Promise((resolve, failure) => 
	{
		let req = http.request({
				hostname, port, path, method: "GET", headers: {
					"Content-Type": "application/json",
					"Content-Length": string.length,
				}
			},
			(res) => 
			{
				res.on("data", (data) => resolve(JSON.parse(data)));
			}
		);
		req.on("error", failure);
		req.write(string);
		req.end();
	});
}

function
init_grade_sh()
{
	if (! grade_sh) {
		process.chdir(env.ASSIGNMENT_PATH);
		grade_sh = execFile("bash", ["grade.sh", "-s"]);
		grade_sh.stderr.setEncoding("utf-8");
		grade_sh.stdout.setEncoding("utf-8");
		grade_sh.stderr.on("data",
			(message) =>
			{	
				message.split("\n").map((message) =>
				{
					let type, input_file, score;
					try {
						[type, input_file, score] = JSON.parse(message);
					}
					catch {
						console.log("Failed to parse message");
						return;
					}					
					if (type === null && input_file === null && score === null) {
						if (webview) {
							webview.webview.postMessage({
								event: "onClose",
								data: null,
							})
						}
						gradeable = true;
						return;
					}
	
					if (webview) {
						webview.webview.postMessage({
							event: "onMessage",
							data: [type, input_file, score],
						});
					}
				})
			}
		);
	}
	return grade_sh;
}
