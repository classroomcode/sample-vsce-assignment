/*
A webview runs in its own context with limited permissions This module allows us
to forward service calls and exposes the same interface as the one in
extension.js. There's probably a better way to do it. But at least this is a
working solution. While I was able to import http, the request property is
undefined. Might be a content-security issue.

https://code.visualstudio.com/api/extension-guides/webview
*/

import 'regenerator-runtime/runtime' // support async/await
import * as React from "react"
import * as ReactDOM from "react-dom"
import App from "./src/app"
import {name, services} from "../services"

let vscode = window.vscode;
let service_interface = null;
let calls = {};
let call_id = 0;

function
http_call(service)
{
    let http_calls = {};
    for (let {method} of service.contributes) {
        
        http_calls[method] = (params) =>
        {    
            // TODO Add max timeout
            return new Promise(async (resolve, failure) =>
            {
                /* There's way to resolve this coroutine within the same context.
                So load the call in 'calls' object and wait for a response...*/
                calls[call_id] = [resolve, failure];
                
                vscode.postMessage({event: "call", data: {
                    method,
                    params: params,
                    call_id: call_id++,
                    provider: service.name,
                }});
                
                while (true) {
                    // Keep this coroutine alive
                    await new Promise((r) => setTimeout(r, 16));
                }
            });
        }
    }
    return http_calls;
}

function
service_api()
{
    if (! service_interface) {
        service_interface = {
            __on:{},
            on(event, handler)
            {
                event = "on" + event;
                if (! service_interface.__on[event]) {
                    service_interface.__on[event] = [];
                }
                service_interface.__on[event].push(handler);
            }
        };
        for (let service of services) {
            if (service.type === "http") {
                service_interface[service.name] = http_call(service);
            }
            else if (service.type === "ws") {
                /* TODO
                We can probably handle websocket calls in webview context. If not
                the same as above pattern applies.
                */
            }
            else if (service.type === "stream") {    
                //Doable if we can forward execFile instance from  extension.
            }
        }
    }
    return service_interface;
}

/* For messages sent from extension*/
window.addEventListener("message", (event) =>
{
    let message = event.data;
    switch (message.event) {
        case "return":
            /* Now that we've received the return value from the extension we
            can invoke our resolvers.  */
            let {call_id, data, error} = message.data;
            let [resolve, failure] = calls[call_id];
            delete calls[call_id];
            
            if (error) {
                failure(data);
            }
            else {
                resolve(data);
            }    
            break;
        default:
            if (service_api().__on[message.event]) {
                for (let handler of service_api().__on[message.event]) {
                    handler(message.data);
                }
            }
    }
});

window.service_api = service_api();
window.service_api.grade_sh = {
    grade()
    {
        vscode.postMessage({
            event: "grade",
            data: null,
        });
    },
    diff(filename)
    {
        vscode.postMessage({
            event: "diff",
            data: filename,
        })
    },
    lsof(args=[])
    {
        vscode.postMessage({
            event: "lsof",
            data: args,
        })
    },
    test(args=[])
    {
        vscode.postMessage({
            event: "test",
            data:args,
        })
    }
};

/* HTML document defined in ../extension.js */
ReactDOM.render(<App name={name}/>, document.getElementById("root"));