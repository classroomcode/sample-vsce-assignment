import * as React from "react"
import "./service-container.css"

function
ToggleHidden(props)
{
    return (
        <button 
            className="ToggleHidden"
            onClick={props.onClick}
        >{props.children}</button>)
}

export default
class ServiceContainer extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {
            hidden: false,
        };
        this.on_click = this.on_click.bind(this);
    }

    on_click()
    {
        this.setState({...this.state, hidden: ! this.state.hidden})
    }

    render()
    {
        return (
            <div className="ServiceContainer">
                <ToggleHidden onClick={this.on_click}>
                    {this.state.hidden ? "Show" : "Hide"}
                </ToggleHidden>
                <div className="ServiceName">
                    Service: {this.props.name}
                </div>
                <div className="ServiceWrapper"
                    style={{display: this.state.hidden ? "none" : "block"}}>
                    {this.props.children}
                </div>
            </div>
        )
    }
};