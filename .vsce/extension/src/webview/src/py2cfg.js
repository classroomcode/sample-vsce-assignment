import * as React from "react"
import "./py2cfg.css"

export default
class CFG_Viewer extends React.Component {

    constructor(props) 
    {
        super(props);
        this.state = {
            locked: false,
            interactive: false,
            svg: "",
            zoom: 100,
        };
        this.zoom_in = this.zoom_in.bind(this);
        this.zoom_out = this.zoom_out.bind(this);

        window.service_api.on("DidChangeTextEditorSelection", (event) =>
        {
            event.selections.forEach((selection) => 
            {
                if (event.filepath.endsWith(".py") && 
                    ! this.state.locked) {
                    window.service_api.py2cfg.build_graph({
                        filename: event.filepath,
                        lineno: selection.start.line + 1,
                        interactive: this.state.interactive,
                    }).then(
                        (result) =>
                        {
                            this.setState({...this.state, svg:result})
                        }
                    ).catch(
                        (error) =>
                        {
                            console.log(error);
                        }
                    );
                }
            })
        });
    }

    zoom_in()
    {
        if (this.state.zoom <= 90) {
            this.setState({...this.state, zoom:this.state.zoom + 10});
        }
    }

    zoom_out()
    {
        if (this.state.zoom >= 10) {
            this.setState({...this.state, zoom:this.state.zoom - 10});
        }
    }

    render()
    { 
        return (
            <div className="CFG_Viewer">
                <div className="ControlBar">
                    <button 
                        className="CtlBt"
                        onClick={
                            ()=>
                            {
                                this.setState({
                                    ...this.state,
                                    locked:! this.state.locked
                                    
                                })
                            }
                        }
                    >
                        {this.state.locked ? "Unlock": " Lock "}
                    </button>
                    <button
                        className="CtlBt"
                        onClick={
                            () =>
                            {
                                this.setState({
                                    ...this.state,
                                    interactive: ! this.state.interactive
                                });
                            }
                        }
                    >
                        {this.state.interactive ? "Normal" : "Interactive"}
                    </button>
                    <button 
                        className="CtlBt" 
                        onClick={this.zoom_in}> + </button>
                    <button
                        className="CtlBt"
                        onClick={this.zoom_out}> - </button>
                </div>
                <div 
                    className="SVG_Wrapper"
                    style={{zoom: `${this.state.zoom}%`}}
                    dangerouslySetInnerHTML={{__html: this.state.svg}}
                />
            </div>
        )
    }
};