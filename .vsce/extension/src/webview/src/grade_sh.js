import * as React from "react"
import * as ReactDOM from "react-dom"
import "./grade_sh.css"


const ALL=0
const STDIO_TEST=1
const TYPECHECK_TEST=2
const FORMAT_TEST=3
const ARG_TEST=4    // Not yet implemented
const UNIT_TEST=5   // Not yet implemented

export default
class GradeSH extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {
            stdio: {},
            arg: {},
            unittest:{},
            format_score: 0,
            type_score: 0,
            total: 0,
            waiting: false,
        }
        window.service_api.on("Diff", ({input, diff}) =>
        {
            let stdio = {...this.state.stdio};
            for (let k in stdio) {
                stdio[k][2] = ""; // clear diff
            }
            stdio[input][2] = diff;
            this.setState({
                ...this.state,
                stdio,
            });
        });
        
        window.service_api.on("Message", ([type, input, score]) =>
        {
            console.log(type, input, score);
            switch (type) {
                case ALL: {
                    this.setState({...this.state, total:score, waiting:false});
                    break;
                }
                case STDIO_TEST: {
                    let stdio = {...this.state.stdio};
                    stdio[input] = [score, false, ""];
                    this.setState({...this.state, stdio});
                    break;
                }
                case TYPECHECK_TEST: {
                    this.setState({...this.state, type_score: score});
                    break;
                }
                case FORMAT_TEST: {
                    this.setState({...this.state, format_score: score});
                    break;
                }
                default: {
                    console.log("grade.sh: NOT IMPLEMENTED");
                }
            }
        });
        window.service_api.on("Close", () =>
        {
            console.log("End Transmission");
        });
        window.service_api.grade_sh.lsof()
    }

    render()
    {
        return (
            <div className="GradeSH">
                <div>
                    <TotalScore>{
                        this.state.waiting ? "..." : this.state.total
                    }</TotalScore>
                    {
                        Object.entries(
                            this.state.stdio
                        ).map(([input, state], id) => 
                        {    
                            let [score, waiting, diff] = state;
                            return <StdioTest 
                                key={id}
                                input={input}
                                diff={diff}
                                onTest={
                                    () =>
                                    {
                                        let stdio = {...this.state.stdio};
                                        stdio[input] = [score, true, ""];
                                        this.setState({
                                            ...this.state,
                                            stdio,
                                            waiting:true
                                        });
                                    }
                                }
                                onDiff={
                                    () =>
                                    {
                                        let stdio = {...this.state.stdio};
                                        if (! stdio[input][2]) {
                                            window.service_api.grade_sh.diff(
                                                input
                                            );
                                        }
                                        stdio[input][2] = ""
                                        this.setState({
                                            ...this.state,
                                            stdio,
                                        })
                                    }
                                }
                            >{
                                waiting ? "..." : score
                            }</StdioTest>
                        })
                    }
                </div>
            </div>
        )
    }
}

function
GradeButton(props)
{
    return (
        <button
                className="GradeButton"
                onClick={props.onClick}>
            Grade
        </button>)
}

function
DiffButton(props)
{
    return <button className="DiffButton"
        onClick={props.onClick}>Diff</button>
}

function
TestButton(props)
{
    return (
        <button className="TestButton" onClick={props.onClick}>
            {props.children}
        </button>
    )
}

function
ScoreDiv(props)
{
    return <div className="ScoreDiv">{props.children}</div>
}

function
TestLabel(props)
{
    return <div className="TestLabel">{props.children}</div>
}


function 
DiffDiv(props)
{
    if (! props.children) {
        return <div className="DiffDiv"/>
    }
    return <div className="DiffDiv">
        <DiffFrame>{props.children}</DiffFrame>
    </div>
}
class DiffFrame extends React.Component {
    
    constructor(props)
    {
        super(props)
        this.state = {
            height: "0px",
        }
        this.ref = React.createRef();
    }

    componentDidMount()
    {
        let iframe = this.ref.current;
        iframe.addEventListener("load", () =>
        {
            this.setState({
                height: iframe.contentWindow.document.body.scrollHeight+"px"
            })
        })
    }

    render()
    {
        return <iframe 
            style={{
                height:this.state.height,
                overflow:"auto",
            }}
            ref={this.ref}
            className="DiffFrame"
            srcdoc={this.props.children}
            height={this.state.height}
        />
    }
}

function
StdioTest(props)
{
    return <div className="StdioTest">
        <DiffButton onClick={
            () =>
            {
                props.onDiff();
            }}/>
        <TestButton onClick={
            () =>
            {   
                window.service_api.grade_sh.test(["-c", props.input]);
                props.onTest();
            }
        }>Test</TestButton>
        <TestLabel>
            {props.input}
        </TestLabel>
        <ScoreDiv>
            {props.children}
        </ScoreDiv>
        <DiffDiv>
            {props.diff}
        </DiffDiv>
    </div>;
}



function
FormatTest()
{

}

function
TypeCheckTest()
{
    
}

function
UnitTest(props)
{
    return <div className="UnitTest">
        <DiffButton onClick={
            () =>
            {
                window.service_api.grade_sh.diff(props.input);
            }}/>
        <TestButton>Test</TestButton>
        <TestLabel>{props.input}</TestLabel>
        <ScoreDiv>{props.score}</ScoreDiv>
    </div>
}

function
TotalScore(props)
{
    return <div id="TotalScore">{props.children}</div>
}