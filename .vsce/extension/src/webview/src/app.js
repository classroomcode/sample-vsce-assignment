import * as React from "react"
import CFG_Viewer from "./py2cfg"
import Minimal from "./minimal"
import ServiceContainer from "./service-container"
import "./app.css"
import GradeSH from "./grade_sh"

class Selection extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {
            filename: "N/A",
            lineno: "-1",
            svg: "",
        }
        window.service_api.on("DidChangeTextEditorSelection", (event) =>
        {
            event.selections.forEach((selection) => {
                this.setState({...this.state,
                    filename: event.filename,
                    lineno: selection.start.line + 1,
                });
            })
        });
    }

    render()
    {
        return (
            <div className="Selection">
                @ {this.state.filename} : {this.state.lineno}
            </div>
        )
    }
}

export default function
App(props)
{
    return (
        <div className="App">
            {props.name}
            <Selection />
            <ServiceContainer name="py2cfg">
                <CFG_Viewer />
            </ServiceContainer>
            <ServiceContainer name="minimal-vsce-microservice">
                <Minimal />
            </ServiceContainer>
            <ServiceContainer name="grade.sh">
                <GradeSH />
            </ServiceContainer>
        </div>
    )
}
