import * as React from "react"

export default
class Minimal extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {html: ""}
        window.service_api["minimal-vsce-microservice"].hello().then(
            (data) => this.setState({html: data})
        );
    }

    render()
    {
        return (<div className="Minimal">{this.state.html}</div>)
    }
    
};