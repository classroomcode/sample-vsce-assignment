#!/bin/bash

URL=$1
NAME=$2
git clone $URL $NAME;
cd $NAME;

if [ -d makefile ]; then
    make -j
fi;

exit 0