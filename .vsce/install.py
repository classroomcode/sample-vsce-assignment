#! ./env/bin/python3
import subprocess
import json
import os
import shlex

SERVICES_JS = f"{os.environ['ABS_EXTENSION_PATH']}/src/services.js"
PACKAGE_JSON = f"{os.environ['ABS_EXTENSION_PATH']}/package.json"

def validate(x):
    # TODO actually validate
    return True

def validate_manifest(x):
    # TODO
    return True

# Defined by toplevel makefile
if "ASSIGNMENT_NAME" not in os.environ:
    print("Unknown assignment")
    exit(1)

with open("services.json", "r") as fp:
    data = json.load(fp)

if not validate(data):
    print("Bad schema")
    exit(1)

version = data["version"]
services = data["services"]

if not os.path.exists("tmp"):
    os.mkdir("tmp")

curdir = os.getcwd()
os.chdir("./tmp")
for service in services:

    service_folder = os.path.basename(
        service["url"].replace("https://", "").replace(".git", "")
    )
    if "name" not in service:
        service["name"] = service_folder
    
    if not os.path.exists(service_folder):
        subprocess.call(["git", "clone", service["url"], service["name"]])

    os.chdir(service["name"])
    if not os.path.exists("service-manifest.json"):
        print(f"{service['name']} missing service-manifest.json")
        os.chdir("../")
        # TODO handle invalid service
        continue
    
    build_error = subprocess.call(["make"])
    if build_error:
        # TODO handle build error
        pass
    
    with open("service-manifest.json", "r") as fp:
        manifest = json.load(fp)
    
    if not validate_manifest(manifest):
        print("Bad schema")
        exit(1)
    
    # Don't know if it's a python server but if it is, run it in virtual env
    activate = [".", os.environ["ASSIGNMENT_PATH"] + "/.vsce/env/bin/activate"]
    shell_args = list(
        os.path.expandvars(x) # Expand any provided shell vars
        for x in manifest["run"] + service.get("args", [])
    )
    # Extend the service entry with run-time parameters
    service["path"] = os.getcwd()
    service["contributes"] = manifest["contributes"]
    service["type"] = manifest["type"]
    # This gets exec'd by nodejs when extension starts
    service["run"] = shlex.join(activate) + " && " + shlex.join(shell_args)
    os.chdir("../")

os.chdir(curdir)
data["name"] = os.environ["ASSIGNMENT_NAME"]
data["path"] = os.environ["ABS_EXTENSION_PATH"]
data["start_command"] = f"extension.{os.environ['ASSIGNMENT_NAME']}"
data["env"] = {
    "ASSIGNMENT_PATH": os.environ["ASSIGNMENT_PATH"]
}

# create service manifest accessible within the extension
with open(SERVICES_JS, "w") as fp:
    fp.write(f"module.exports = {json.dumps(data)}")

with open("extension/package.json", "r") as fp:
    package = json.load(fp)

# bind extension name to top level directory name
package["name"] = package["displayName"] = os.environ["ASSIGNMENT_NAME"]
# bind command name to top level directory name
package["contributes"]["commands"].append({
    "command": data["start_command"],
    "title": "Assignment: " + os.environ["ASSIGNMENT_NAME"]
})

# update template package.json file
with open(PACKAGE_JSON, "w") as fp:
    json.dump(package, fp, indent=4)