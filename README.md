# Braking Down the Problem

For this assignment you will create a simple car diagnostics program.
We provide the diagnostics methodology below.
You could even use this program in real life if you are having issues with your car starting.
This program will test your understanding of conditional branching structures in Python3.

Your program has to be named `mechanic.py`

## Visual Studio Code
If you are using Visual Studio Code, try our extension!
```bash
$ make -j
```
And reload window by pressing `Ctrl + Shift + P` and searching for "Developer: Reload Window" in the prompt.

## Inputs and Pre-conditions

Proper inputs for this program include:
- 'y' for Yes
- 'n' for No
- Anything else is invalid.
If the user enters anything other than y or n, then bother them to enter y or n, until they do.
You will need to use a loop to accomplish this.

## Outputs and Post-conditions
Outputs to this program will be interactive print statements, and a final printed diagnosis.
Follow these steps to write your program:

Start with a simple introduction prompting the user for input on whether or not they would like to perform diagnostics on their program (y for yes, n for no)
- If a user selects Yes, then you'll start asking them questions (tests) that follow the logic of the diagram below.
    - Print out each test, and retrieve the answer.
    - Follow the diagram until you get to a diagnosis suggestion (a terminal bubble at the end of a chain of decisions).
    - Once the user reaches a final diagnosis suggestion, print it out.

Every question should be addressed by writing an if-else question.
For example:

![You're a special cookie](CookieFlowGraph_cfg.svg)

```py
yesno = input("Do you like chocolate")
if yesno == "y":
    choctype = input("Do you like dark chocolate?")
    if choctype == "y":
        print("Here's a cookie")
    else:
        print("Too bad")
else:
    print("You're a smarty")
```

## Input Checking
When programming, I would follow a process like this:
- start with the initial prompt, verify it works
- next, add only one branch, verify it works.
- then, progressively add small components, verifying each, one at a time.

## Reminder
Remember, this is INDEPENDENT work.
Loose inspiration from a friend is ok, but if you collaborate to the degree that your code is the same, we call that cheating.

## Diagnostic Flow Chart:

![society.bottomtext](pa01.gif)

## Grading and Testing
Just like on our first assignment:
* Run the `grade.sh` script on your own machine.
* If it does not show a 100, your code is broken, and you need to fix it.
* To see what is being tested, look in the folders found in the `./stdio_tests/` folder to see what your code should do.
* After it shows your code is working, you are **almost** done.
* Make sure you commit and push your code to the remote git repository just like pa00, using the command line, NOT the web interface.
* Finally, verify that your Pipeline is green and passing in the git-classes web interface.
* If you don't understand any of the above steps, read the syllabus again, stop by office hours, or ask in the Zulip chat.
* The desired text is the text we give you in the `./stdio_tests/`, whether or not it is grammaticall correct, spelled correctly, whatever. You just need to make your output match out output, using the correct mechanism.
