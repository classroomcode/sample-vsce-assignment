#!/usr/bin/python3
# -*- coding: utf-8 -*-

def get_user_input():
    accepted = {"y", "n"}
    while True:
        user_in = input().lower()
        if user_in in accepted:
            yield user_in
        else:
            print("Please select Y to continue or N to quit")

user_input = get_user_input()

print("Would you like to perform diagnostics on your car? y/n")
if next(user_input) == "y":
    print("Test number 1: Starter cranks?")
    if next(user_input) == "n":
        print("Test number 2: Starter spins?")
        if next(user_input) == "n":
            print("Test number 3: Battery read over 12V?")
            if next(user_input) == "y":
                print("Test number 4: Cleaned terminals?")
                if next(user_input) == "y":
                    pass
                else:
                    print("Diagnosis is:  Clean battery terminals and "
                        "connectors, engine ground."
                    )

