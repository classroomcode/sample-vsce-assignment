Would you like to perform diagnostics on your car? y/n
Test number 1: Starter cranks?
Test number 2: Starter spins?
Test number 3: Battery read over 12V?
Test number 4: Cleaned terminals?
Diagnosis is:  Clean battery terminals and connectors, engine ground.
